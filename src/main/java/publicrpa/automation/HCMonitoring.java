package publicrpa.automation;

import java.io.IOException;
import java.net.URL;

import publicrpa.automation.monitoring.Monitoring;

public class HCMonitoring extends Monitoring {

	final private String hcUrl;

	public HCMonitoring(Configuration config) {
		super(config);
		this.hcUrl = config.get("hc-ping");
	}

	@Override
	public void start() {
		try {
			new URL(hcUrl + "/start").openConnection().getContentLength();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void finish() {
		try {
			new URL(hcUrl).openConnection().getContentLength();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
