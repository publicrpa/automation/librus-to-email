package publicrpa.automation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Collectors;

import publicrpa.automation.email.EmailChannel;
import publicrpa.view.Screenshot;
import publicrpa.view.librus.Message;
import publicrpa.view.librus.RodzinaPage;

public class LibrusToEmail extends AbstractBrowserAutomation {

	final private String login;
	final private String password;
	final private String student;
	final private EmailChannel emailChannel;

	public LibrusToEmail(String jsonString) {
		super(jsonString);
		this.emailChannel = new EmailChannel(config);
		this.login = config.get("login");
		this.password = config.get("password");
		this.student = config.get("student");
	}

	@Override
	protected void taskSpecificFlow() {

        var wiadomosci = new ArrayList<Message>();
        var ogloszenia = new ArrayList<Message>();
        var uwagi = new ArrayList<Message>();

        new RodzinaPage()
	        	.load()
	        	.navigateToLoginPage()
	        	.login(login, password)
	        	.checkForNewAnnouncements(ogloszenia)
				.navigateToWiadomosci()
				.getUnreadMessages(wiadomosci)
				.navigateToUwagi()
				.getUnreadMessages(uwagi);

        wiadomosci.forEach(m -> emailChannel.sendEmails("Nowa Wiadomość na LIBRUS - " + student, m.getText()));
        ogloszenia.forEach(m -> emailChannel.sendEmails("Nowe Ogłoszenie na LIBRUS - " + student, m.getText()));
        uwagi.forEach(m -> emailChannel.sendEmails("Nowa UWAGA!!! na LIBRUS - " + student, m.getText()));

	}

	public static void main(String[] args) throws IOException {
		new LibrusToEmail(readConfigFile(args)).run();
	}

	private static String readConfigFile(String[] args) throws IOException {
		return Files.lines(Paths.get(args[0])).collect(Collectors.joining());
	}

	@Override
	protected void handleExpection(Exception e) {
        e.printStackTrace();
        emailChannel.sendEmailWithException(e, Screenshot.getQueue());
	}

}
